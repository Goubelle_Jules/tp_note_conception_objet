﻿using System;


namespace Compte
{

    public class Compteur
    {
        private static int valeur = 0;

        public int Incremente()
        {
            valeur = valeur + 1;
            return valeur;
        }
        public int Decremente()
        {
            valeur = valeur - 1;
            return valeur;
        }
        public int RAZ()
        {
            valeur = 0;
            return valeur;
        }
        public int affiche()
        {
            return valeur;
        }
    }
}

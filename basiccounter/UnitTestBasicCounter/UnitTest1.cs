﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Compte;


namespace UnitTestBasicCounter
{
    [TestClass]
    public class UnitTest1
    {

       
        [TestMethod]
        public void TestIncremente()
        {
            Compteur nombre = new Compteur();
            Assert.AreEqual(1,nombre.Incremente());
            Assert.AreNotEqual(3, nombre.Incremente());
        
        }

       

        [TestMethod]
        public void TestRAZ()
        {
            Compteur nombre = new Compteur();
            Assert.AreEqual(0, nombre.RAZ());
            Assert.AreNotEqual(2,nombre.RAZ());
           
        }

        [TestMethod]
        public void TestAffiche()
        {
            Compteur nombre = new Compteur();
            Assert.AreEqual(0, nombre.Affiche());
            Assert.AreNotEqual(2,nombre.Affiche());
            
        }

        [TestMethod]
        public void TestDecremente()
        {
            Compteur nombre = new Compteur();


            Assert.AreEqual(-1, nombre.Decremente());
            Assert.AreNotEqual(0, nombre.Decremente());

        }


    }
}

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RAZ = New System.Windows.Forms.Button()
        Me.plus = New System.Windows.Forms.Button()
        Me.moins = New System.Windows.Forms.Button()
        Me.affcompte = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'RAZ
        '
        Me.RAZ.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.RAZ.Location = New System.Drawing.Point(433, 314)
        Me.RAZ.Margin = New System.Windows.Forms.Padding(4)
        Me.RAZ.Name = "RAZ"
        Me.RAZ.Size = New System.Drawing.Size(159, 46)
        Me.RAZ.TabIndex = 0
        Me.RAZ.Text = "RAZ"
        Me.RAZ.UseVisualStyleBackColor = True
        '
        'plus
        '
        Me.plus.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.plus.Location = New System.Drawing.Point(669, 202)
        Me.plus.Margin = New System.Windows.Forms.Padding(4)
        Me.plus.Name = "plus"
        Me.plus.Size = New System.Drawing.Size(159, 46)
        Me.plus.TabIndex = 1
        Me.plus.Text = "+"
        Me.plus.UseVisualStyleBackColor = True
        '
        'moins
        '
        Me.moins.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.moins.Location = New System.Drawing.Point(197, 202)
        Me.moins.Margin = New System.Windows.Forms.Padding(4)
        Me.moins.Name = "moins"
        Me.moins.Size = New System.Drawing.Size(159, 46)
        Me.moins.TabIndex = 2
        Me.moins.Text = "-"
        Me.moins.UseVisualStyleBackColor = True
        '
        'affcompte
        '
        Me.affcompte.AutoSize = True
        Me.affcompte.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!)
        Me.affcompte.Location = New System.Drawing.Point(481, 202)
        Me.affcompte.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.affcompte.Name = "affcompte"
        Me.affcompte.Size = New System.Drawing.Size(53, 58)
        Me.affcompte.TabIndex = 3
        Me.affcompte.Text = "0"
        Me.affcompte.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.Label2.Location = New System.Drawing.Point(467, 98)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 25)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Total :"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1067, 554)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.affcompte)
        Me.Controls.Add(Me.moins)
        Me.Controls.Add(Me.plus)
        Me.Controls.Add(Me.RAZ)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RAZ As Button
    Friend WithEvents plus As Button
    Friend WithEvents moins As Button
    Friend WithEvents affcompte As Label
    Friend WithEvents Label2 As Label
End Class
